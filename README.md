# camerastatus

a Project for showmewebcam raspberry pi webcams

## Requirements

* Rust (see https://www.rust-lang.org/tools/install but I'd advise against piping curl stuff directly into your shell!)
* `cross` to cross-compile to arm (for the raspberry pi) with `cargo install cross`
* cross requires docker or podman and it also requires your user to be able to run docker containers, see https://docs.docker.com/engine/install/linux-postinstall/

## Building / Deploying

* `cross build --target arm-unknown-linux-gnueabihf --release`
* `ssh pi@<hostname> killall camerastatus`
* `scp target/arm-unknown-linux-gnueabihf/release/camerastatus pi@<hostname>:<somewhere>`
* `ssh pi@<hostname> <somewhere>/camerastatus`

## Todo
- [ ] actually display something useful (yes, this is the big one)
- [ ] start automatically from systemd
- [ ] how to integrate into showmewebcam

