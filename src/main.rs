use embedded_graphics::{
    mono_font::{ascii::FONT_6X10, MonoTextStyle},
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{
        PrimitiveStyle, PrimitiveStyleBuilder, StrokeAlignment,
    },
    text::{Alignment, Text}
};

use linux_embedded_hal::I2cdev;
use machine_ip;
use sh1106::{prelude::*, Builder};

use std::thread::sleep;
use std::time::Duration;

fn main() {
    let i2c = I2cdev::new("/dev/i2c-1").unwrap();
    let mut disp: GraphicsMode<_> = Builder::new()
        .with_size(DisplaySize::Display128x64)
        .connect_i2c(i2c)
        .into();
    disp.init().unwrap();
    disp.flush().unwrap();

    let character_style = MonoTextStyle::new(&FONT_6X10, BinaryColor::On);

    let border_stroke = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::On)
        .stroke_width(3)
        .stroke_alignment(StrokeAlignment::Inside)
        .build();
    let thin_stroke = PrimitiveStyle::with_stroke(BinaryColor::On, 1);


    loop {
        let local_addr = machine_ip::get().unwrap();
        let formatted = format!("IP: {}", local_addr.to_string());
        disp
            .bounding_box()
            .into_styled(border_stroke)
            .draw(&mut disp);

        Text::with_alignment(
            "ohai",
            disp.bounding_box().center() + Point::new(0, -15),
            character_style,
            Alignment::Center,
        )
        .draw(&mut disp);

        Text::with_alignment(
            &formatted,
            disp.bounding_box().center() + Point::new(0, 15),
            character_style,
            Alignment::Center,
        )
        .draw(&mut disp);

        disp.flush().unwrap();
        sleep(Duration::from_secs(1));
    }
}
